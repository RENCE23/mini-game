var app = new Vue({  
	el: "#app",     
	
	data: {			
		health:100,
		level: 1,
		started: false, 
		ended: false,
		startTime: "",
		elapsedTime: "",
		seconds: "00",

	},
	methods:{  
		
		start : function(){   
			self = this;     
			this.startTime = Date.now();
			this.started = true;
			setInterval(function(){
			    self.regenerate();
			}, 1500); 
			setInterval(function(){
			    self.countTimer();
			},100);
		},
		
		punch: function(){   
			if(this.started == true)
			{   
				this.health -= 2; 
				if(this.health <= 0){
					this.ended = true;
				}
			}else{
				alert("Please click start to play!");  
			}
			
		},
		
		regenerate: function(){ 
			if(this.health <= 90 && this.ended == false) 
			{
				this.health += parseInt(this.level);
			}
		},
		
		restart: function(){   
			this.health = 100;
			this.ended = false;
			this.started = false;
			this.seconds = "00";
			this.startTime = "";
			this.elapsedTime = "";

		},
		
		countTimer: function() {      
		    if(this.started == true && this.ended == false)
		    {
		    	this.elapsedTime = Date.now() - this.startTime;
		    	this.seconds = (this.elapsedTime / 1000).toFixed(3);
		    }
		},
		
	},
});